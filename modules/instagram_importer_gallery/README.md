## The instagram importer gallery plug-and-play solution

Download the libary here: https://github.com/stef-van-looveren/instagram-importer-gallery

1. Enable the *Instagram importer gallery* module, available in the instagram importer module.
2. Place the javascript plugin inside your *libraries* folder, so that `jquery.instagram-importer-gallery.js` can be found in `web/libraries/instagram-importer-gallery/jquery.instagram-importer-gallery.js`.
3. Download the [PhotoSwipe](https://github.com/dimsemenov/PhotoSwipe) library and place it in your *libraries* folder, so that `photoswipe.js` can be found in `web/libraries/photoswipe/dist/photoswipe.js`.
4. If you do not get a warning in the status reports page (*/admin/reports/status*) everything is installed correctly.
5. Make sure you have imported your first instagram posts with the [Instagram importer](https://www.drupal.org/project/instagram_importer) module.
6. Blocks of type *Instagram Feed Gallery Block* are now available to add to a region. In the block form you can specify which posts (from a hashtag or a user) should be shown.
7. Note that you should not place multiple blocks of this type on the same page. You can mix hashtags/user posts in one block, but not add multiple blocks to one page.

When experiencing problems, contact me at https://github.com/stef-van-looveren.

Read this tutorial if you want to add the package to composer: https://stefvanlooveren.me/blog/how-composer-require-github-package
