<?php

namespace Drupal\instagram_importer_gallery\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'Instagram Feed Gallery' Block.
 *
 * @Block(
 *   id = "instagram_importer_gallery_block",
 *   admin_label = @Translation("Instagram Feed Gallery Block"),
 *   category = @Translation("Instagram Feed Gallery Block"),
 * )
 */
class InstagramFeedGalleryBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {

    $hashtags = \Drupal::config('instagram_importer.settings')->get('settings.instagram_tags');
    if (strlen(trim($hashtags)) > 1) {
      $hashtags = str_replace(' ', '', $hashtags);
      $hashtags = str_replace('#', '', $hashtags);
      if (strpos($hashtags, ',') !== false) {
        $hashtags = explode(',', $hashtags);
        $options = ['0', 'None'];
        foreach($hashtags as $hashtag) {
          $options[str_replace('.','%',$hashtag)] = $hashtag;
        }
      } else {
        $options = [str_replace('.','%',$hashtags) => $hashtags];
        array_splice( $options, 0, 0, 'None' );
      }
      $form['hashtags'] = array(
        '#type' => 'radios',
        '#title' => $this->t('Choose the Instagram hashtags you want to see'),
        '#description' => $this->t('To add some, head to /admin/config/instagram/settings'),
        '#options' => $options,
        '#default_value' => isset($this->configuration['hashtags']) ? $this->configuration['hashtags'] : null
      );
    }

    $users = \Drupal::config('instagram_importer.settings')->get('settings.instagram_users');
    if (strlen(trim($users)) > 1) {
      $users = str_replace(' ', '', $users);
      $users = str_replace('#', '', $users);
      $options = ['0' => 'None'];
      if (strpos($users, ',') !== false) {
        $users = explode(',', $users);
        foreach($users as $user) {
          $options[str_replace('.','%',$user)] = $user;
        }
      } else {
        $options = [str_replace('.','%',$users) => $users];
        array_splice( $options, 0, 0, 'None' );
      }
      $form['users'] = array(
        '#type' => 'radios',
        '#title' => $this->t('Choose the Instagram user profile posts you want to see'),
        '#description' => $this->t('To add some, head to /admin/config/instagram/settings'),
        '#options' => $options,
        '#default_value' => isset($this->configuration['users']) ? $this->configuration['users'] : null
      );
    }


    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    if ($form_state->hasAnyErrors()) {
      return;
    }
    else {
      $this->configuration['users'] = $form_state->getValue('users');
      $this->configuration['hashtags'] = $form_state->getValue('hashtags');
    }
  }

  /**
   * {@inheritdoc)
   */
  public function blockValidate($form, FormStateInterface $form_state) {
    $user = $form_state->getValue('users');
    $hashtag = $form_state->getValue('hashtags');
    // Can't both be empty
    $notset = false;
    if(!isset($hashtag) && !isset($user)) {
      $notset = true;
    }
    // May not both be "none"
    $both_none = 0;
    if($user === "0") {
      $both_none += 1;
    }
    if($hashtag === "0") {
      $both_none += 1;
    }
    $both_set = false;
    if((strlen($user) > 1) && (strlen($hashtag) > 1)) {
      $both_set = true;
    }
    
    if($notset || ($both_none > 1) || $both_set) {
      $form_state->setErrorByName('hashtags', t('You can not combine hashtags and users in one block. Please select "None" for at least 1.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $users = $this->configuration['users'];
    $users_sanitized = null;
    if($users) {
      if($users != "0") {
        $users_sanitized = str_replace("%", ".", $users);
      }
    }

    $hashtags = $this->configuration['hashtags'];
    $hashtags_sanitized = null;
    if($hashtags) {
      if($hashtags != "0") {
        $hashtags_sanitized = str_replace("%", ".", $hashtags);
      }
    }

    return [
      '#children' => '
<div class="ifg-loading"></div><div id="instagram-feed-gallery" data-users="'.$users_sanitized.'" data-hashtags="'.$hashtags_sanitized.'"></div>
           <!-- Root element of PhotoSwipe. Must have class pswp. -->
<div class="pswp photoswipe-gallery" tabindex="-1" role="dialog" aria-hidden="true">

    <!-- Background of PhotoSwipe. 
         It\'s a separate element as animating opacity is faster than rgba(). -->
    <div class="pswp__bg"></div>

    <!-- Slides wrapper with overflow:hidden. -->
    <div class="pswp__scroll-wrap">

        <!-- Container that holds slides. 
            PhotoSwipe keeps only 3 of them in the DOM to save memory.
            Don\'t modify these 3 pswp__item elements, data is added later on. -->
        <div class="pswp__container">
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
        </div>

        <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
        <div class="pswp__ui pswp__ui--hidden">

            <div class="pswp__top-bar">

                <!--  Controls are self-explanatory. Order can be changed. -->

                <div class="pswp__counter"></div>

                <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>

                <button class="pswp__button pswp__button--share" title="Share"></button>

                <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>

                <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

                <!-- Preloader demo https://codepen.io/dimsemenov/pen/yyBWoR -->
                <!-- element will get class pswp__preloader--active when preloader is running -->
                <div class="pswp__preloader">
                    <div class="pswp__preloader__icn">
                      <div class="pswp__preloader__cut">
                        <div class="pswp__preloader__donut"></div>
                      </div>
                    </div>
                </div>
            </div>

            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                <div class="pswp__share-tooltip"></div> 
            </div>

            <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
            </button>

            <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
            </button>

            <div class="pswp__caption">
                <div class="pswp__caption__center"></div>
            </div>

        </div>

    </div>

</div>
      ',
      '#attached' => array(
        'library' => array(
          'instagram_importer_gallery/gallery',
        ),
      ),
    ];
  }

}
