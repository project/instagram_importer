<?php

namespace Drupal\instagram_importer\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class InstagramSettingsForm.
 *
 * @package Drupal\instagram_importer\Form
 */
class InstagramSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'instagram_importer_admin_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'instagram_importer.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $moduleHandler = \Drupal::service('module_handler');
    if ($moduleHandler->moduleExists('instagram_importer_gallery')){
      if (!file_exists('libraries/instagram-importer-gallery/jquery.instagram-importer-gallery.js') || !file_exists('libraries/photoswipe/dist/photoswipe.js')) {
        \Drupal::messenger()->addWarning('Instagram importer gallery or photoswipe library missing. See the readme.md file of the module or the project page for instructions.');
      }
    }

    $config = $this->config('instagram_importer.settings');

    $form['instagram_tags'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Instagram hashtags to fetch'),
      '#default_value' => $config->get('settings.instagram_tags'),
      '#description' => $this->t('Add your hashtags here f.e. "#drupal". Comma seperate for multiple hashtags to follow.'),
    ];

    $form['instagram_users'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Instagram users to fetch'),
      '#default_value' => $config->get('settings.instagram_users'),
      '#description' => $this->t('Add your instagram users here f.e. "dries.buytaert". Do not add the "@" sign. Comma seperate for multiple hashtags to follow.'),
    ];

    $form['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable importing of new posts on cron run.'),
      '#default_value' => $config->get('settings.enabled'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('instagram_importer.settings');
    $config->set('settings.instagram_tags', $form_state->getValue('instagram_tags'));
    $config->set('settings.instagram_users', $form_state->getValue('instagram_users'));
    $config->set('settings.enabled', $form_state->getValue('enabled'));

    $config->save();
    parent::submitForm($form, $form_state);
  }

}
