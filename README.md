# Instagram importer

This module exists out of one main module and one sub-module. 

The main module Instagram importer will be used for fetching the correct hashtags/users and scraping new posts on cron run. It creates nodes of type Instagram. From there, you will be able to create your own views.

For users who like a more out-of-the-box solution for this can enable the *instagram importer gallery* submodule. Note that you need a paid additional javascript library to install called [*Instagram importer gallery*](https://enjoy.gitstore.app/repositories/stef-van-looveren/instagram-importer-gallery). It will create a beautiful mobile-ready gallery of your feeds. Instructions on how-to can be found in te submodule's readme.md file.

## Getting started

Enable the module and go to */admin/config/instagram/settings* to choose your settings. You can add both (multiple) instagram users and hashtags

Run cron with *drush cron* or go to *admin/config/system/cron*. After this, new nodes of type 'instagram' should be imported. Make sure *automated cron* is enabled, or define custom cronjobs. Updates are only imported during cron run!

You can now build your own views to show the imported images in the way you would handle galleries normally. 

When experiencing problems, contact me at https://github.com/stef-van-looveren.
